#!/usr/bin/env node
// ^ usar intérprete node.js para este archivo

// usar módulo fastify
const fastify = require('fastify')

// puerto como variable de entorno o sino uno predeterminado
const PORT = process.env.PORT || '8080'

// instanciar fastify con logs activados
const app = fastify({
  logger: true,
})

// primer ruta
app.get('/', async (request, reply) => {
  return { hello: 'world' }
})

// ruta con URL bonita con función definida mas adelante
app.get('/nice/route/:name', niceRoute)

// ruta POST con mas parámetros definidos posteriormente
// en forma predeterminada entrada y salida son JSON
app.post('/simplepost', simplePost)

// servidor en escucha
// 0.0.0.0 escucha el servicio en todas las IP y dominios posibles
app.listen(PORT, '0.0.0.0')

// función para ruta bonita
async function niceRoute(request, reply) {
  const name = request.params.name
  return { hello: name }
}

// función para método POST
async function simplePost(request, reply) {
  const num = request.body.num
  const word = request.body.word
  const another = request.body.another
  return { 
    num,
    word,
    another,
  }
}
