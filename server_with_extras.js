const fastify = require('fastify')
const oas = require('fastify-oas')

const PORT = process.env.PORT || '8080'

// configuración general para OpenAPI
const oasGeneral = {
  routePrefix: '/documentation',
  swagger: {
    info: {
      title: 'Test openapi',
      description: 'testing the fastify swagger api',
      version: '0.2.0',
    },
    externalDocs: {
      url: 'https://swagger.io',
      description: 'Find more info here',
    },
    consumes: ['application/json'],
    produces: ['application/json'],
  },
  exposeRoute: true,
}

// Esquema de validación y serialización de ruta POST
const simplePostSchema = {
  summary: 'It is just a simple post', // optional
  description: 'It is just a simple post, but description', // optional
  operationId: 'simplepost', // optional
  tags: ['MyTag'], // optional
  body: {
    type: 'object',
    properties: {
      num: {
        type: 'integer',
        description: 'just a number',
      },
      word: {
        type: 'string',
        description: 'just a word',
      },
      another: {
        type: 'string',
        description: 'just another word',
      },
    },
    required: [
      'num',
      'word',
    ],
    example: {
      num: 123,
      word: 'Matias',
      another: 'no one cares',
    },
  },
  response: {
    200: {
      description: 'Same input parameters',
      type: 'object',
      properties: {
        num: {
          type: 'integer',
          description: 'just a number',
        },
        word: {
          type: 'string',
          description: 'just a word',
        },
        another: {
          type: 'string',
          description: 'just another word',
        },
      },
    }
  },
}

const app = fastify({
  logger: true,
})

app.ready((err) => {
  if (err) throw err
  app.oas()
})

app.register(oas, oasGeneral)

app.get('/', async (request, reply) => {
  return { hello: 'world' }
})

app.get('/nice/route/:name', niceRoute)

app.post('/simplepost', { schema: simplePostSchema }, simplePost)

// 0.0.0.0 escucha el servicio en todas las IP y dominios posibles
app.listen(PORT, '0.0.0.0')

async function niceRoute(request, reply) {
  const name = request.params.name
  return { hello: name }
}

async function simplePost(request, reply) {
  const num = request.body.num
  const word = request.body.word
  const another = request.body.another
  return { 
    num,
    word,
    another,
  }
}
